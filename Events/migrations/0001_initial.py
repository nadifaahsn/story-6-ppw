# Generated by Django 3.1.2 on 2020-10-20 04:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_event', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Participant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('participant', models.CharField(max_length=200)),
                ('nama_event', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='Events.event')),
            ],
        ),
    ]
