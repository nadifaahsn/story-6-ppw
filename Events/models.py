from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
class Event(models.Model):
    nama_event = models.CharField(max_length=200,  blank=True)

    def __str__(self):
        return "{}".format(self.nama_event)

class Participant(models.Model):
    nama_event = models.ForeignKey(Event, on_delete=models.CASCADE)
    participant = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return "{}.{}".format(self.nama_event, self.participant)