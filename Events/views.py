from django.shortcuts import render
from django.http import HttpResponseRedirect
# Create your views here.
from django.shortcuts import render

from .forms import EventForm, ParticipantForm
from .models import Event, Participant
# Create your views here.
def index(request):
    nama_event = Event.objects.all()
    event = []
    for nama in nama_event:
        participant = Participant.objects.filter(nama_event_id= nama)
        participant_list =[]
        for peserta in participant:
            participant_list.append(peserta)
        event.append((nama, participant_list))
    
    
    participant_form = ParticipantForm(request.POST)
    if request.method == 'POST':
        if 'participant' in request.POST and 'id_name_event' in request.POST:
            if participant_form.is_valid():
                event = Event.objects.get(id=request.POST['id_name_event'])                
                data = participant_form.cleaned_data
                data_event = Participant()
                data_event.participant = data['participant']
                data_event.nama_event = event
                data_event.save()
                return HttpResponseRedirect("/")
    context = {
        'event' : event,
        'participant_form' : participant_form,
    }

    return render(request, 'Events/index.html', context)

def create_event(request):
    event_form = EventForm(request.POST)
    if request.method == 'POST':
        if 'nama_event' in request.POST:
            if event_form.is_valid():
                event_form.save()
                return HttpResponseRedirect("/")

    context = {
        'event_form' : event_form,
    }
    return render(request, 'Events/create.html', context)