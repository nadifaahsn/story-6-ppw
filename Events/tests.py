from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from Events.models import Event, Participant

class TestEvents(TestCase):
    def test_url_events(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_url_create(self):
        response = Client().get('/create')
        self.assertEqual(response.status_code,200)

    def test_template_events(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'Events/index.html')

    def test_template_create(self):
        response = Client().get('/create')
        self.assertTemplateUsed(response, 'Events/create.html')

    def test_model_create_event(self):
        Event.objects.create(nama_event='event_test')
        total = Event.objects.all().count()
        self.assertEqual(total,1)

    def test_model_create_participant(self):
        event = Event.objects.create(nama_event='event_test')
        Participant.objects.create(participant='participant_test', nama_event=event)
        total = Participant.objects.all().count()
        self.assertEqual(total,1)

    def test_model_create_nama_event(self):
        Event.objects.create(nama_event='Event')
        event = Event.objects.get(nama_event='Event')
        self.assertEqual(str(event),'Event')

    def test_model_create_nama_participant(self):
        event = Event.objects.create(nama_event="event_test")
        participant = Participant.objects.create(participant="participant_test", nama_event=event)
        self.assertEqual(str(participant),"event_test.participant_test")

    def test_model_save_POST_request_event(self):
        response = Client().post('/create', data={'nama_event':'Story 6'})
        total = Event.objects.filter(nama_event='Story 6').count()
        self.assertEqual(total,1)
        self.assertEqual(response.status_code, 302)
    
    def test_model_save_POST_request_participant(self):
        Event.objects.create(nama_event="event_test")
        response = Client().post('/', data={'id_name_event' : '1', 'participant' : 'Hasna'})
        total = Participant.objects.all().count()
        self.assertEqual(total,1)
        self.assertEqual(response.status_code, 302)